﻿using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ThirdParty
{
    public static class ShortUrlHelper
    {
        /// <summary>
        /// 新浪转换短链接
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string Convert_SINA_Short_Url(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return "";
            }

            //api地址
            const string ADDRESSS = "http://api.t.sina.com.cn/short_url/shorten.json?source=2815391962";

            GC.Collect();
            try
            {
                string address = ADDRESSS + "&url_long=" + HttpUtility.UrlEncode(url);
                using (WebClient w = new WebClient())
                {
                    var json = w.DownloadString(address);
                    var urls = JsonConvert.DeserializeObject<List<sina_short_url>>(json);

                    if (urls != null && urls.Count > 0)
                    {
                        return urls[0].url_short;
                    }
                }
            }
            catch (Exception)
            {

            }

            return "";
        }

        /// <summary>
        /// 新浪转换短链接-批量
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static List<sina_short_url> Convert_SINA_Short_Url_ByUrlList(List<string> urlList)
        {
            if (urlList.Count > 20)
            {
                throw new Exception("最大支持20个url转换");
            }
            var address = "http://api.t.sina.com.cn/short_url/shorten.json?source=2815391962";
            foreach (var url in urlList)
            {
                address += "&url_long=" + HttpUtility.UrlEncode(url);
            }
            using (WebClient w = new WebClient())
            {
                var json = w.DownloadString(address);
                var urls = JsonConvert.DeserializeObject<List<sina_short_url>>(json);

                if (urls != null && urls.Count > 0)
                {
                    return urls;
                }
            }

            return null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class sina_short_url
    {
        /// <summary>
        /// 
        /// </summary>
        public string url_short { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string url_long { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int type { get; set; }
    }
}
