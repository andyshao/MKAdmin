﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MKAdmin.DTO.Web.Common;

namespace MKAdmin.DTO.Web.Organization.PermissionSetting
{
    public class GetPermissionAllotListParameter : PagingParameter
    {
        /// <summary>
        /// 被修改人账号Id
        /// </summary>
        public int updatorAgentId { get; set; }
    }
}
