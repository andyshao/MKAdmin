﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Organization.PermissionSetting
{
   public class RoleDelParameter
    {
        public int RoleId { get; set; }
    }
}
