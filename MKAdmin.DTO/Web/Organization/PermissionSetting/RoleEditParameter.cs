﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Organization.PermissionSetting
{
    public class RoleEditParameter
    {
        public int RoleId { get; set; }
    }

    public class RoleEditSaveParameter: RoleEditParameter
    {
        public string RoleName { get; set; }
        public string Remark { get; set; }
        public List<int> RightIdList { get; set; }
    }

    public class GetRoleRightModel
    {
        public string RoleName { get; set; }
        public string Remark { get; set; }
        public List<int> RightIdList { get; set; }
    }
}
