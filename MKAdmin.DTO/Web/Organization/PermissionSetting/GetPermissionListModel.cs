﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Organization.PermissionSetting
{
    public class GetPermissionListModel
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string Creator { get; set; }
        public string CreateTime { get; set; }
    }
}
