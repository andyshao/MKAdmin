﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Organization.PermissionSetting
{
    public class GetPermissionDataListModel
    {
        /// <summary>
        /// 菜单权限集合
        /// </summary>
        public List<PermissionMenuDataModel> menuData { get; set; }
        /// <summary>
        /// 功能权限集合
        /// </summary>
        public List<int> functionData { get; set; }
    }

    /// <summary>
    /// 菜单权限实体
    /// </summary>
    public class PermissionMenuDataModel
    {
        /// <summary>
        /// 权限ID
        /// </summary>
        public int rightId { get; set; }
        /// <summary>
        /// 父Id
        /// </summary>
        public int rightParentId { get; set; }
        /// <summary>
        /// 权限名称
        /// </summary>
        public string rightName { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int orderNo { get; set; }
        /// <summary>
        /// 资源名称
        /// </summary>
        public string resourceName { get; set; }
        /// <summary>
        /// 页面路径
        /// </summary>
        public string menuPageUrl { get; set; }
        /// <summary>
        /// 图标名称
        /// </summary>
        public string iconName { get; set; }
    }
}
