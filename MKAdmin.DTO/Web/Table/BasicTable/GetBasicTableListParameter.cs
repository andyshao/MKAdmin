﻿using MKAdmin.DTO.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Table.BasicTable
{
    public class GetBasicTableListParameter : PagingParameter
    {
        public string name { get; set; }
    }
}
