﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.Login
{
    public class GetSubAccountParameter
    {
        /// <summary>
        /// 
        /// </summary>
        public int AgentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public EmAccountStatusCode AccountStatusCode { get; set; } = EmAccountStatusCode.NormalOnly;
    }

    /// <summary>
    /// 账号状态
    /// </summary>
    public enum EmAccountStatusCode
    {
        /// <summary>
        /// 只包含正常的
        /// </summary>
        NormalOnly = 0,
        /// <summary>
        /// 只包含禁用的
        /// </summary>
        ForbiddenOnly = 1,
        /// <summary>
        /// 正常和禁用的
        /// </summary>
        NorAndForbidden = 2,
        /// <summary>
        /// 正常、禁用和删除的
        /// </summary>
        All = 3
    }
}
