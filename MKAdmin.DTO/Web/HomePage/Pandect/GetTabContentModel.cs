﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.HomePage.Pandect
{
    public class GetTabContentModel
    {
        /// <summary>
        /// tabId
        /// </summary>
        public int TabId { get; set; }
        /// <summary>
        /// tab标题
        /// </summary>
        public string TabContentTitle { get; set; }
        /// <summary>
        /// 源代码类型(Html,JScript,Css,CSharp)
        /// </summary>
        public string CodeType { get; set; }
        /// <summary>
        /// Tab图标
        /// </summary>
        public string TabIcon { get; set; }
        /// <summary>
        /// tab内容
        /// </summary>
        public string TabContentStr { get; set; }
    }
}
