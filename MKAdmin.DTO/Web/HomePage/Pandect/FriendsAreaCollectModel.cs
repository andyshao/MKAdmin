﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.HomePage.Pandect
{
    public class FriendsAreaCollectModel
    {
        /// <summary>
        /// x轴数据
        /// </summary>
        public List<int> xAxis { get; set; }
        /// <summary>
        /// y轴数据
        /// </summary>
        public List<string> yAxis { get; set; }
    }
}
