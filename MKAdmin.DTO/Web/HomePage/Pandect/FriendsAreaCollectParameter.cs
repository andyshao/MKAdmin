﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MKAdmin.DTO.Web.Common;

namespace MKAdmin.DTO.Web.HomePage.Pandect
{
    public class FriendsAreaCollectParameter : PagingParameter
    {
        /// <summary>
        /// 省份
        /// </summary>
        public string prov { get; set; }
    }
}
