﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.DTO.Web.File
{
    public class OnlinePreviewModel
    {
        //预览文件可访问路径
        public string PreviewFileUrl { get; set; }
    }
}
