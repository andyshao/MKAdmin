﻿//图片裁剪上传
layui.use(['jquery', 'croppers'], function () {
    var $ = layui.jquery,
        croppers = layui.croppers;

    //裁剪上传
    croppers.render({
        elem: '#el_fileCropUpload'
        , mark: 1 / 1    //裁剪比例(宽高比例)
        , url: "/home/UploadFile"  //
        , data: {
            uploadfile_ant: 'Crop'
        }
        , done: function (json) { //上传完毕回调
            //alert(JSON.stringify(json));
            var data = JSON.parse(json);
            console.log(data);
            $("#imgFileCropPreview").attr('src', data.data.url);
        }
    });
});