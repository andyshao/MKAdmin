﻿layui.use(['form', 'dialog', 'element', 'ajaxUtil'], function () {
    var ajaxutil = layui.ajaxUtil
        , form = layui.form
        , element = layui.element
        , dialog = layui.dialog;

    $(function () {
        //判断一下当前是不是顶层，如果不是，则做一下顶层页面重定向
        if (window != top) {
            top.location.href = location.href;
        }

        //SyntaxHighlighter.all();
    });

    //下载源码
    $(".download-sound-code").click(function () {
        ajaxutil.request({
            async: false,
            url: '/homepage/pandect/downloadcodelog',
            success: function (res) {

            }
        });
        alert('暂不支持下载源码,敬请期待...');
    });

    //查看源代码
    $('.layui-tab-see-sound-code').click(function () {
        dialog.open({
            dialogId: 'cn_tab_view_soundcode',
            title: '源代码',
            width: 1000,
            height: 550,
            ok: null,
            cancel: null
        })


        //清除tab
        $("#cn_tab_view_soundcode .layui-tab-title").empty();
        $("#cn_tab_view_soundcode .layui-tab-content").empty();

        var layId = $(".layui-tab-title .layui-this").attr('lay-id');

        //获取tab内容
        ajaxutil.request({
            async: false,
            url: '/homepage/pandect/gettabcontent',
            data: {
                TabId: layId
            },
            success: function (res) {
                console.log(res);

                if (res != null && res.data != null) {
                    $.each(res.data, function (index, item) {
                        item.TabContentStr = item.TabContentStr.replace(/&/g, "&amp;");
                        item.TabContentStr = item.TabContentStr.replace(/</g, "&lt;");
                        item.TabContentStr = item.TabContentStr.replace(/>/g, "&gt;");
                        item.TabContentStr = item.TabContentStr.replace(/ /g, "&nbsp;");
                        item.TabContentStr = item.TabContentStr.replace(/\'/g, "&#39;");
                        item.TabContentStr = item.TabContentStr.replace(/\"/g, "&quot;");

                        //alert(item.TabContentStr);
                        //新增一个Tab项
                        element.tabAdd('demo', {
                            title: '<img src="' + item.TabIcon + '"/>' + ' ' + item.TabContentTitle
                            , content: '<pre class="brush:' + item.CodeType + ';toolbar:false">' + item.TabContentStr + '</pre>'
                            , id: item.TabId + '_' + (index + 1)
                        })

                        //需要执行下高亮方法
                        SyntaxHighlighter.highlight();
                    });

                    //默认第一个选中
                    $("#cn_tab_view_soundcode .layui-tab-title li:first").click();
                }

            }
        });


    });
});
