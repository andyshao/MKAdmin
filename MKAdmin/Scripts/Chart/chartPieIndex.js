﻿//表格管理 - 基本表格
layui.use(['jquery', 'table', 'ajaxUtil', 'permissionUtil', 'switchTab'], function () {
    var table = layui.table,
        $ = layui.jquery,
        ajaxutil = layui.ajaxUtil,
        permissionUtil = layui.permissionUtil,
        switchTab = layui.switchTab;
    //permissionUtil.setting(100000);
    var myChart;

    var loadBasicLine = function () {
        // 基于准备好的dom，初始化echarts实例
        if (myChart !== null && myChart !== undefined) {
            myChart.clear();
        }
        myChart = echarts.init(document.getElementById('sn_dv_chartPie_main'));

        var option = {
            title: {
                text: '员工学历人数统计'
                , x: 'center'
            },
            tooltip: {
                trigger: 'item'
                , formatter: function (params) {
                    var name = params.name;
                    var value = params.value;
                    var marker = params.marker;
                    //console.log(params)
                    return name + '<br/> ' + marker + value + '人';
                }
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ['初中', '高中', '大专', '本科', '博士']
            },
            series: [{
                data: [
                    { value: 820, name: '初中' },
                    { value: 932, name: '高中' },
                    { value: 901, name: '大专' },
                    { value: 934, name: '本科' },
                    { value: 1290, name: '博士' }
                ]
                , type: 'pie'
            }]
        };

        myChart.setOption(option, true);
    }

    loadBasicLine();
    $('#btn_ChartPie_Refresh').click(function () {
        loadBasicLine();
    });


});