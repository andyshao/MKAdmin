﻿//登录
layui.use(['form', 'layedit', 'laydate', 'jquery'], function () {
    var form = layui.form,
        layer = layui.layer,
        $ = layui.jquery;

    $(function () {
        //判断一下当前是不是做顶层，如果不是，则做一下顶层页面重定向
        if (window != top) {
            top.location.href = location.href;
        }

        //alert('开始了');
    });

    //自定义验证规则
    form.verify({
        title: function (value) {
            if (value.length < 5) {
                return '标题至少得5个字符啊';
            }
        },
        password: [/(.+){6,12}$/, '密码必须6到12位'],
        verity: [/(.+){6}$/, '验证码必须是6位'],

    });
        $('.User_login_btn').focus(function () {
       
        $(this).parents('.m-login-warp').find('.zf-logo-forget-tip-erro').hide()
    })
    form.on('checkbox(remember)', function (data) {
        if (data.elem.checked) {
            $("#IsRemember").val("true");
        }
        else {
            $("#IsRemember").val("");
        }
    });   
    //监听提交
    form.on('submit(login)', function (data) {
        //layer.alert(JSON.stringify(data.field), {
        //    title: '最终的提交信息'
        //})

        $("#frmLogin").submit();
        //return false;
    });
});