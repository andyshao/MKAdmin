﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using MKAdmin.DTO.Web.Common;
using MKAdmin.ToolKit;

namespace MKAdmin.Web.Authorizes
{
    public class Loginer
    {
        /// <summary>
        /// 
        /// </summary>
        private const string SPLIT_CODE = "#$(%";

        /// <summary>
        /// 
        /// </summary>
        private const string USER_DATA_FORMAT = "{0}{1}{2}{3}{4}";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="remember"></param>
        public static void SignIn(UserInfoModel info, bool remember = false)
        {
            DateTime timeoutDate = DateTime.Now.AddHours(7);
            FormsAuthenticationTicket frmTicket = new FormsAuthenticationTicket(
                   19,
                   AuthConfig.NAME,
                   DateTime.Now,
                   timeoutDate,
                   false,
                   GetUserData(info, AuthConfig.ROLE_ADMIN));
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(frmTicket));
            if (remember)
                cookie.Expires = timeoutDate;

            cookie.HttpOnly = true;
            HttpContext.Current.Response.Cookies.Add(cookie);

            HttpContext.Current.Session[AuthConfig.SESSION_LOGIN_KEY] = info;
        }

        /// <summary>
        /// 登出
        /// </summary>
        public static void SignOut()
        {
            HttpCookie cookie = HttpContext.Current.Response.Cookies[FormsAuthentication.FormsCookieName];
            cookie.Value = null;
            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Set(cookie);

            if (HttpContext.Current.Session != null)
                HttpContext.Current.Session.Abandon();

            //int userID = CurrentUser.AgentId;

            FormsAuthentication.SignOut();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operatorNO"></param>
        /// <returns></returns>
        private static string GetUserData(UserInfoModel info, string roleName)
        {
            string data = string.Format(USER_DATA_FORMAT, info.OperatorId, SPLIT_CODE
                 , info.OperatorNo, SPLIT_CODE, info.OperatorName);
            //Guid.NewGuid().ToString("N")HttpContext.Current.Session.SessionID

            return CommonUtilHelper.SymmEncryption(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal static UserInfoModel AnalysisTicketData()
        {
            UserInfoModel info = null;
            try
            {
                FormsIdentity identity = HttpContext.Current.User.Identity as FormsIdentity;
                if (identity != null)
                { 
                    info = new UserInfoModel();
                    string data = CommonUtilHelper.SymmDecryption(identity.Ticket.UserData);
                    string[] arrData = data.Split(new string[] { SPLIT_CODE }, StringSplitOptions.None);

                    info.OperatorId = int.Parse(arrData[0]);
                    info.OperatorNo = arrData[1];
                    info.OperatorName = arrData[2];
                    return info;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.ToString());
            }
            return info;
        }
    }
}