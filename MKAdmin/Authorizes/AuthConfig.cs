﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKAdmin.Web.Authorizes
{
    public class AuthConfig
    {
        public const string ROLE_ADMIN = "UseeOperator_WXG";

        public const string NAME = "USee_WXG";

        public const string SESSION_LOGIN_KEY = "WXG_Login_User";
    }
}