﻿using MKAdmin.ToolKit;
using MKAdmin.ToolKit.FileKit;
using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.File.Controllers
{
    public class PreviewController : BaseMvcController
    {
        // GET: File/Preview
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.FilePreviewIndex);
        }

        /// <summary>
        /// 预览文件
        /// </summary>
        /// <param name="key"></param>
        /// <param name="catalogName"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult OnliePreview(string catalogName)
        {
            var catalogConfig = FileCatalogHelper.Catalog["Preview"];

            var filePath = $"{catalogConfig.DestFile.AbsolutePath}/{catalogName}";
            var relativePath = $"{catalogConfig.DestFile.RelativePath}/{catalogName}";
            var result = OnlinePreviewHelper.OfficeDocumentToHtml(filePath, relativePath);


            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}