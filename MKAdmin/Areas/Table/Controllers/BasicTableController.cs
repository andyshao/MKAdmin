﻿using MKAdmin.DTO.Web.Common;
using MKAdmin.DTO.Web.Table.BasicTable;
using MKAdmin.IService.Web.Table;
using MKAdmin.ToolKit;
using MKAdmin.Web.App_Start;
using MKAdmin.Web.Authorizes;
using MKAdmin.Web.Controllers;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.Table.Controllers
{
    /// <summary>
    /// 基本表格
    /// </summary>
    public class BasicTableController : BaseMvcController
    {
        [Inject]
        public IBasicTableService BasicTableService { set; get; }

        // GET: Table/BasicTable
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.BasicTableIndex);
        }

        [HttpPost]
        public JsonResult List(GetBasicTableListParameter parameter)
        {
            LogHelper.Info("测试日志...");
            var result = BasicTableService.List(parameter);

            return Json(result, JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public JsonResult Add(AddBasicTableParameter parameter)
        {
            var result = new Result();
            if (!string.IsNullOrEmpty(parameter.employeeName))
            {
                if (parameter.employeeAge > 0 && parameter.employeeAge < 150)
                {
                    if (parameter.employeeSex > 0)
                    {
                        if (parameter.employeeHeight > 0 && parameter.employeeHeight < 300)
                        {
                            if (!string.IsNullOrEmpty(parameter.employeeEducation))
                            {
                                if (!string.IsNullOrEmpty(parameter.employeePhone))
                                {
                                    var userInfo = CurrentUser.GetLoginInfo();
                                    result = BasicTableService.Add(parameter, userInfo);
                                }
                                else
                                {
                                    result.msg = "请输入联系方式";
                                    result.status = false;
                                }
                            }
                            else
                            {
                                result.msg = "请选择学历";
                                result.status = false;
                            }
                        }
                        else
                        {
                            result.msg = "请输入身高";
                            result.status = false;
                        }
                    }
                    else
                    {
                        result.msg = "请选择性别";
                        result.status = false;
                    }
                }
                else
                {
                    result.msg = "请输入正确的年龄";
                    result.status = false;
                }
            }
            else
            {
                result.msg = "请输入姓名";
                result.status = false;
            }
            return Json(result, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult Edit(EditBasicTableParameter parameter)
        {
            var result = new Result();
            if (parameter.employeeId > 0)
            {
                if (!string.IsNullOrEmpty(parameter.employeeName))
                {
                    if (parameter.employeeAge > 0 && parameter.employeeAge < 150)
                    {
                        if (parameter.employeeSex > 0)
                        {
                            if (parameter.employeeHeight > 0 && parameter.employeeHeight < 300)
                            {
                                if (!string.IsNullOrEmpty(parameter.employeeEducation))
                                {
                                    if (!string.IsNullOrEmpty(parameter.employeePhone))
                                    {
                                        result = BasicTableService.Edit(parameter);
                                    }
                                    else
                                    {
                                        result.msg = "请输入联系方式";
                                        result.status = false;
                                    }
                                }
                                else
                                {
                                    result.msg = "请选择学历";
                                    result.status = false;
                                }
                            }
                            else
                            {
                                result.msg = "请输入身高";
                                result.status = false;
                            }
                        }
                        else
                        {
                            result.msg = "请选择性别";
                            result.status = false;
                        }
                    }
                    else
                    {
                        result.msg = "请输入正确的年龄";
                        result.status = false;
                    }
                }
                else
                {
                    result.msg = "请输入姓名";
                    result.status = false;
                }
            }
            else
            {
                result.msg = "请提供员工编号";
                result.status = false;
            }
            return Json(result, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Del(DelBasicTableParameter parameter)
        {
            var result = new Result();

            if (parameter.employeeId > 0)
            {
                result = BasicTableService.Del(parameter);
            }
            else
            {
                result.msg = "请提供员工编号";
                result.status = false;
            }
            return Json(result, JsonRequestBehavior.DenyGet);

        }
    }
}