﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MKAdmin.DTO.Web.HomePage.Pandect;
using MKAdmin.IService.Web.HomePage;
using MKAdmin.Web.Authorizes;
using MKAdmin.Web.Controllers;
using System.IO;
using System.Text;
using System.Reflection;
using MKAdmin.ToolKit;

namespace MKAdmin.Web.Areas.HomePage.Controllers
{
    public class PandectController : BaseMvcController
    {
        [Inject]
        public IPandectService PandectService { set; get; }

        // GET: HomePage/Pandect
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取首页汇总数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetTabContent(GetTabContentParameter parameter)
        {
            var result = PandectService.GetTabContent(parameter);

            return Json(result, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// 好友区域统计
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetFriendAreaCollect(FriendsAreaCollectParameter parameter)
        {
            var userInfo = CurrentUser.GetLoginInfo();
            var result = PandectService.GetFriendAreaCollect(parameter, userInfo);
            return Json(result, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult DownLoadCodeLog()
        {
            var userInfo = CurrentUser.GetLoginInfo();
            var result = PandectService.DownLoadCodeLog(userInfo);
            return Json(result, JsonRequestBehavior.DenyGet);
        }
    }
}