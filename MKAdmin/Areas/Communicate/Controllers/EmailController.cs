﻿using MKAdmin.DTO.Web.Common;
using MKAdmin.ToolKit;
using MKAdmin.Web.App_Start;
using MKAdmin.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static MKAdmin.ToolKit.EmailHelper;

namespace MKAdmin.Web.Areas.Communicate.Controllers
{
    public class EmailController : BaseMvcController
    {
        // GET: Communicate/Email
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.CommunicateEmailIndex);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SendEmail(MailInfo parameter)
        {
            var result = new Result();
            if (parameter.To == null || parameter.To.Count == 0)
            {
                result.status = false; ;
                result.msg = "请填写收件人邮箱";
            }
            else if (string.IsNullOrEmpty(parameter.Subject))
            {
                result.status = false; ;
                result.msg = "请填写主题";
            }
            else if (string.IsNullOrEmpty(parameter.Body))
            {
                result.status = false; ;
                result.msg = "请填写邮件内容";
            }
            else
            {
                result = EmailHelper.Send(new EmailHelper.MailInfo()
                {
                    To = parameter.To,
                    Subject = parameter.Subject,
                    Body = parameter.Body,
                    //Body = "<p style=\"text-align:center;\"><b>353543</b></p>",
                    IsBodyHtml = true,
                    Async = false
                });
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}