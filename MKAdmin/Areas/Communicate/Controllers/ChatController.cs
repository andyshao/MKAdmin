﻿using MKAdmin.Web.App_Start;
using MKAdmin.Web.Authorizes;
using MKAdmin.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Areas.Communicate.Controllers
{
    public class ChatController : BaseMvcController
    {
        // GET: Communicate/Chat
        public ActionResult Index()
        {
            return View(PageViewFilesConfig.CommunicateChatIndex);
        }

        [HttpGet]
        public ActionResult GetUserInfo()
        {
            var userInfo = CurrentUser.GetLoginInfo();

            var loginInfo = new
            {
                userId = userInfo.OperatorId,
                userType = userInfo.OperatorId == 10 ? 1 : 0
            };

            return Json(loginInfo, JsonRequestBehavior.AllowGet);
        }
    }
}