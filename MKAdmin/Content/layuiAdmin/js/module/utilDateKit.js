﻿layui.define(function (exports) {
    var utilDateKit = (function () {
        var that = {};

        that.getNow = function () {
            var day = new Date();
            var Year = 0;
            var Month = 0;
            var Day = 0;
            var CurrentDate = "";
            //初始化时间 
            //Year= day.getYear();//有火狐下2008年显示108的bug 
            Year = day.getFullYear(); //ie火狐下都可以 
            Month = day.getMonth() + 1;
            Day = day.getDate();
            //Hour = day.getHours(); 
            // Minute = day.getMinutes(); 
            // Second = day.getSeconds(); 
            CurrentDate += Year + "-";
            if (Month >= 10) {
                CurrentDate += Month + "-";
            } else {
                CurrentDate += "0" + Month + "-";
            }
            if (Day >= 10) {
                CurrentDate += Day;
            } else {
                CurrentDate += "0" + Day;
            }
            return CurrentDate;
        }

        //+---------------------------------------------------  
        //| 日期计算  
        //+---------------------------------------------------  
        that.add = function (date, strInterval, Number) {
            var dtTmp = date;
            switch (strInterval) {
                case 's':
                    return new Date(Date.parse(dtTmp) + (1000 * Number));
                case 'n':
                    return new Date(Date.parse(dtTmp) + (60000 * Number));
                case 'h':
                    return new Date(Date.parse(dtTmp) + (3600000 * Number));
                case 'd':
                    return new Date(Date.parse(dtTmp) + (86400000 * Number));
                case 'w':
                    return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number));
                case 'q':
                    return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
                case 'm':
                    return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
                case 'y':
                    return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
            }
        };

        //+---------------------------------------------------  
        //| 比较日期差 dtEnd 格式为日期型或者有效日期格式字符串  
        //+---------------------------------------------------  
        that.diff = function (date, strInterval, dtEnd) {
            var dtStart = new Date(date);
            if (typeof dtEnd == 'string') //如果是字符串转换为日期型  
            {
                dtEnd = that.StringToDate(dtEnd);
            }
            switch (strInterval) {
                case 's':
                    return parseInt((dtEnd - dtStart) / 1000);
                case 'n':
                    return parseInt((dtEnd - dtStart) / 60000);
                case 'h':
                    return parseInt((dtEnd - dtStart) / 3600000);
                case 'd':
                    return parseInt((dtEnd - dtStart) / 86400000);
                case 'w':
                    return parseInt((dtEnd - dtStart) / (86400000 * 7));
                case 'm':
                    return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - dtStart.getFullYear()) * 12) - (dtStart.getMonth() + 1);
                case 'y':
                    return dtEnd.getFullYear() - dtStart.getFullYear();
            }
        };

        //+---------------------------------------------------  
        //| 字符串转成日期类型   
        //| 格式 MM/dd/YYYY MM-dd-YYYY YYYY/MM/dd YYYY-MM-dd  
        //+---------------------------------------------------  
        that.StringToDate = function (DateStr) {
            var converted = Date.parse(DateStr);
            var myDate = new Date(converted);
            if (isNaN(myDate)) {
                //var delimCahar = DateStr.indexOf('/')!=-1?'/':'-';  
                var arys = DateStr.split('-');
                myDate = new Date(arys[0], --arys[1], arys[2]);
            }
            return myDate;
        }

        that.getIntervalDate = function (date, interval) {
            //获取系统时间
            var baseDate = new Date(Date.parse(date.replace(/-/g, "/")));
            var baseYear = baseDate.getFullYear();
            var baseMonth = baseDate.getMonth();
            var baseDate = baseDate.getDate();
            //处理
            var newDate = new Date(baseYear, baseMonth, baseDate);
            newDate.setDate(newDate.getDate() + interval); //取得系统时间的相差日期,interval 为负数时是前几天,正数时是后几天
            var temMonth = newDate.getMonth();
            temMonth++;
            var lastMonth = temMonth >= 10 ? temMonth : ("0" + temMonth)
            var temDate = newDate.getDate();
            var lastDate = temDate >= 10 ? temDate : ("0" + temDate)
            //得到最终结果
            newDate = newDate.getFullYear() + "-" + lastMonth + "-" + lastDate;
            return newDate;
        }

        //---------------------------------------------------  
        // 日期格式化  
        // 格式 YYYY/yyyy/YY/yy 表示年份  
        // MM/M 月份  
        // W/w 星期  
        // dd/DD/d/D 日期  
        // hh/HH/h/H 时间  
        // mm/m 分钟  
        // ss/SS/s/S 秒  
        //---------------------------------------------------  
        that.format = function (date, formatStr) {
            date = new Date(date);

            var str = formatStr;
            var Week = ['日', '一', '二', '三', '四', '五', '六'];

            str = str.replace(/yyyy|YYYY/, date.getFullYear());
            str = str.replace(/yy|YY/, (date.getYear() % 100) > 9 ? (date.getYear() % 100).toString() : '0' + (date.getYear() % 100));

            var month = date.getMonth() == 0 ? 1 : date.getMonth() + 1;
            str = str.replace(/MM/, month > 9 ? month.toString() : '0' + month);
            str = str.replace(/M/g, month);

            str = str.replace(/w|W/g, Week[date.getDay()]);

            str = str.replace(/dd|DD/, date.getDate() > 9 ? date.getDate().toString() : '0' + date.getDate());
            str = str.replace(/d|D/g, date.getDate());

            str = str.replace(/hh|HH/, date.getHours() > 9 ? date.getHours().toString() : '0' + date.getHours());
            str = str.replace(/h|H/g, date.getHours());
            str = str.replace(/mm/, date.getMinutes() > 9 ? date.getMinutes().toString() : '0' + date.getMinutes());
            str = str.replace(/m/g, date.getMinutes());

            str = str.replace(/ss|SS/, date.getSeconds() > 9 ? date.getSeconds().toString() : '0' + date.getSeconds());
            str = str.replace(/s|S/g, date.getSeconds());

            return str;
        };

        //+---------------------------------------------------  
        //| 日期合法性验证  
        //| 格式为：YYYY-MM-DD或YYYY/MM/DD  
        //+---------------------------------------------------  
        that.isValidDate = function (DateStr) {
            var sDate = DateStr.replace(/(^\s+|\s+$)/g, ''); //去两边空格;   
            if (sDate == '') return true;
            //如果格式满足YYYY-(/)MM-(/)DD或YYYY-(/)M-(/)DD或YYYY-(/)M-(/)D或YYYY-(/)MM-(/)D就替换为''   
            //数据库中，合法日期可以是:YYYY-MM/DD(2003-3/21),数据库会自动转换为YYYY-MM-DD格式   
            var s = sDate.replace(/[\d]{ 4,4 }[\-/]{ 1 }[\d]{ 1,2 }[\-/]{ 1 }[\d]{ 1,2 }/g, '');
            if (s == '') //说明格式满足YYYY-MM-DD或YYYY-M-DD或YYYY-M-D或YYYY-MM-D   
            {
                var t = new Date(sDate.replace(/\-/g, '/'));
                var ar = sDate.split(/[-/:]/);
                if (ar[0] != t.getYear() || ar[1] != t.getMonth() + 1 || ar[2] != t.getDate()) {
                    //alert('错误的日期格式！格式为：YYYY-MM-DD或YYYY/MM/DD。注意闰年。');   
                    return false;
                }
            } else {
                //alert('错误的日期格式！格式为：YYYY-MM-DD或YYYY/MM/DD。注意闰年。');   
                return false;
            }
            return true;
        };

        // 查询条件 默认日期时间段
        that.getSearchBetweenDate = function () {
            var period = { startDate: "", endDate: "" };

            period.endDate = this.getNow();
            //var dtArr = period.endDate.split("-");
            //var dateNow = new Date(dtArr[0], dtArr[1], dtArr[2]);
            //var lastMonth = this.add(dateNow, 'm', -1);
            //var lastMonth = this.add(lastMonth, 'd', 1);
            period.startDate = this.getIntervalDate(period.endDate, -30); // this.format(lastMonth, 'yyyy-MM-dd');

            return period;
        };

        // 查询条件 默认日期时间段
        that.getSearchThreeMonthsDate = function () {
            var period = { startDate: "", endDate: "" };

            period.endDate = this.getNow();
            //var dtArr = period.endDate.split("-");
            //var dateNow = new Date(dtArr[0], dtArr[1], dtArr[2]);
            //var lastMonth = this.add(dateNow, 'm', -1);
            //var lastMonth = this.add(lastMonth, 'd', 1);
            var day = new Date();
            day.setMonth(day.getMonth() - 3);
            var Year = 0;
            var Month = 0;
            var Day = 0;
            var CurrentDate = "";
            //初始化时间 
            //Year= day.getYear();//有火狐下2008年显示108的bug 
            Year = day.getFullYear(); //ie火狐下都可以 
            Month = day.getMonth() + 1;
            Day = day.getDate();
            //Hour = day.getHours(); 
            // Minute = day.getMinutes(); 
            // Second = day.getSeconds(); 
            CurrentDate += Year + "-";
            if (Month >= 10) {
                CurrentDate += Month + "-";
            } else {
                CurrentDate += "0" + Month + "-";
            }
            if (Day >= 10) {
                CurrentDate += Day;
            } else {
                CurrentDate += "0" + Day;
            }
            period.startDate = CurrentDate;
            //period.startDate = this.getIntervalDate(period.endDate,0); // this.format(lastMonth, 'yyyy-MM-dd');

            return period;
        };

        ///获取一周时间
        that.getWeekSearchBetweenDate = function () {
            var period = { startDate: "", endDate: "" };

            period.endDate = this.getNow();
            //var dtArr = period.endDate.split("-");
            //var dateNow = new Date(dtArr[0], dtArr[1], dtArr[2]);
            //var lastMonth = this.add(dateNow, 'm', -1);
            //var lastMonth = this.add(lastMonth, 'd', 1);
            period.startDate = this.getIntervalDate(period.endDate, -7); // this.format(lastMonth, 'yyyy-MM-dd');

            return period;
        };

        ///根据当前日期获取上一周日期
        that.getLastWeekSearchBetweenDate = function () {
            var period = { startDate: "", endDate: "" };

            var todayDate = new Date();
            if (todayDate.getDay() == 5) {
                period.endDate = this.getIntervalDate(this.getNow(), -5);
                period.startDate = this.getIntervalDate(period.endDate, -6);
            }
            else if (todayDate.getDay() == 6) {
                period.endDate = this.getIntervalDate(this.getNow(), -6);
                period.startDate = this.getIntervalDate(period.endDate, -6);
            }
            else if (todayDate.getDay() == 0) {
                period.endDate = this.getIntervalDate(this.getNow(), -7);
                period.startDate = this.getIntervalDate(period.endDate, -6);
            }
            else if (todayDate.getDay() == 1) {
                period.endDate = this.getIntervalDate(this.getNow(), -1);
                period.startDate = this.getIntervalDate(period.endDate, -6);
            }
            else if (todayDate.getDay() == 2) {
                period.endDate = this.getIntervalDate(this.getNow(), -2);
                period.startDate = this.getIntervalDate(period.endDate, -6);
            }
            else if (todayDate.getDay() == 3) {
                period.endDate = this.getIntervalDate(this.getNow(), -3);
                period.startDate = this.getIntervalDate(period.endDate, -6);
            }
            else if (todayDate.getDay() == 4) {
                period.endDate = this.getIntervalDate(this.getNow(), -4);
                period.startDate = this.getIntervalDate(period.endDate, -6);
            } else {
                period.endDate = this.getNow();
                period.startDate = this.getIntervalDate(period.endDate, -7);
            }
            //period.endDate = this.getNow();
            //var dtArr = period.endDate.split("-");
            //var dateNow = new Date(dtArr[0], dtArr[1], dtArr[2]);
            //var lastMonth = this.add(dateNow, 'm', -1);
            //var lastMonth = this.add(lastMonth, 'd', 1);
            // period.startDate = this.getIntervalDate(period.endDate, -7); // this.format(lastMonth, 'yyyy-MM-dd');

            return period;
        };

        that.validDate = function (startDateObj, endDateObj) {
            var defaultDate = that.getSearchBetweenDate();
            startDateObj.datebox('setValue', defaultDate.startDate);
            endDateObj.datebox('setValue', defaultDate.endDate);

            startDateObj.datebox({
                onSelect: function (date) {
                    var endDate = endDateObj.datebox("getValue");
                    if (endDate == "") {
                        endDate = defaultDate.endDate;
                    }

                    if (that.StringToDate(endDate) < date) {
                        endDateObj.datebox("setValue", that.format(date, 'yyyy-MM-dd'));
                    }
                }
            });

            endDateObj.datebox({
                onSelect: function (date) {
                    var startDate = startDateObj.datebox("getValue");
                    if (startDate == "") {
                        startDate = defaultDate.startDate;
                    }

                    if (that.StringToDate(startDate) > date) {
                        startDateObj.datebox("setValue", that.format(date, 'yyyy-MM-dd'));
                    }
                }
            });

            return true;
        };

        //显示消息发送时间
        //规则：
        //1. 一天以前的消息显示 年/月/日
        //2. 今天内的消息显示 时分
        that.getMenuMsgTime = function (msgTime) {
            var newTime = "";
            try {
                if (msgTime != "" && msgTime != "2000-01-01T00:00:00") {
                    var today = utilDateKit.getNow();

                    var timeFormat = "HH:mm";
                    //一天前的消息格式
                    //console.log("消息时间:" + msgTime + ",当前时间:" + today);
                    if (msgTime < today) {
                        timeFormat = "yy/M/D";
                    };
                    //var timeFormat = "yy/M/D HH:mm";
                    msgTime = msgTime.replace(/T/g, ' ');
                    msgTime = msgTime.replace(/-/g, "/");
                    newTime = utilDateKit.format(msgTime, timeFormat);
                }
            } catch (e) {

            }
            return newTime;
        }
        return that;
    })();

    //输出test接口
    exports('utilDateKit', utilDateKit);
});



