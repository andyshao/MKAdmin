﻿layui.define(['jquery', 'utilDateKit'], function (exports) {
    var $ = layui.jquery,
        UtilDateKit = layui.utilDateKit;

    var msgTemplate = (function () {
        var message = {};

        //表情
        var FaceList = [];

        message.loadMsg = function (content) {
            //获取时间戳
            var msgTimestamp = (new Date()).valueOf();

            //msgTemplate.showMsgSendTime(msg, i, data);

            var msgInfo = '';
            try {
                if (content.typeCode == 0) {
                    switch (content.msgType) {
                        case 0:
                            //文本
                            msgInfo = msgTemplate.getReceiveTextMsg(content);
                            break;
                        case 1:
                            //图片
                            msgInfo = msgTemplate.getReceiveImgMsg(content);
                            break;
                        case 2:
                            //语音
                            //msgInfo = msgTemplate.getReceiveVoiceMsg(content);
                            break;
                        case 3:
                            //视频
                            //msgInfo = msgTemplate.getReceiveVideoMsg(content);
                            break;
                        case 10:
                            //系统消息
                            //msgInfo = msgTemplate.getSystemMsg(content);
                            break;
                        case 10001:
                            //链接
                            //msgInfo = msgTemplate.getReceiveLinkMsg(content);
                            break;
                        case 10002:
                            //小程序
                            //msgInfo = msgTemplate.getReceiveXCXMsg(content);
                            break;
                        case 10003:
                            //名片
                            //msgInfo = msgTemplate.getReceiveCardMsg(content);
                            break;
                        case 10004:
                            //墨迹图
                            //msgInfo = msgTemplate.getReceiveImgMsg(content);
                            break;
                        case 10005:
                            //红包
                            //msgInfo = msgTemplate.getReceiveRedPacketMsg(content);
                            break;
                        case 10006:
                            //转账
                            //msgInfo = msgTemplate.getReceiveTransferMsg(content);
                            break;
                        default:
                            break;
                    }
                }
                else if (content.typeCode == 1) {
                    switch (content.msgType) {
                        case 0:
                            //文本
                            msgInfo = msgTemplate.getSendTextMsg(content);
                            break;
                        case 1:
                            //图片
                            msgInfo = msgTemplate.getSendImgMsg(content);
                            break;
                        default:
                            break;
                    }
                }

            } catch (e) {
                msgInfo = '加载消息失败...';
            }

            $(".GroupChatIndex_InfoShow").append(msgInfo);
        },
            //显示消息发送时间
            //规则：
            //1. 一天以前的消息显示 年月日 时分
            //2. 今天内的消息显示 时分
            //3. 每条消息和上一条消息对比,如果上一条没有消息或和上一条时间相差大于1分钟,则先显示时间,在显示消息
            message.showMsgSendTime = function (msg, i, data) {
                try {

                    var today = UtilDateKit.getNow();

                    var timeFormat = "hh:mm";
                    //一天前的消息格式
                    if (msg.sendTime < today) {
                        timeFormat = "yyyy年M月D日 hh:mm";
                    };

                    msg.sendTime = msg.sendTime.replace(/T/g, ' ');
                    msg.sendTime = msg.sendTime.replace(/-/g, "/");

                    var msgParam = {
                        chatId: msg.shatId,
                        text: UtilDateKit.format(msg.sendTime, timeFormat)
                    };
                    //第一条消息
                    if (i == 0) {
                        //console.log(intervalDay);
                        $(".GroupChatIndex_MsgPanel").append(msgTemplate.getTimeMsg(msgParam));
                    }
                    else {
                        var lastMsgTime = data[i - 1].sendTime;
                        //和上一条消息时间对比
                        var intervalMin2 = UtilDateKit.diff(lastMsgTime, 'n', msg.sendTime);
                        //console.log(intervalMin2);
                        if (intervalMin2 > 10) {
                            $(".GroupChatIndex_MsgPanel").append(msgTemplate.getTimeMsg(msgParam));
                        };
                    }
                } catch (e) {
                    //测试
                    //alert(e);
                }
            },
            message.showHistoryMsgSendTime = function (msg, i, data, msgTimestamp) {
                try {
                    var today = UtilDateKit.getNow();

                    var timeFormat = "hh:mm";
                    //一天前的消息格式
                    if (msg.sendTime < today) {
                        timeFormat = "yyyy年M月D日 hh:mm";
                    };

                    var msgParam = {
                        chatId: msg.chatId,
                        text: UtilDateKit.format(msg.sendTime, timeFormat)
                    };
                    //第一条消息
                    if (i == msg.length - 1) {
                        //console.log(intervalDay);
                        $(".GroupChatIndex_MsgPanel").append(msgTemplate.getTimeMsg(msgParam));
                    }
                    else {
                        var lastMsgTime = data[i + 1].sendTime;
                        //和上一条消息时间对比
                        var intervalMin2 = UtilDateKit.diff(lastMsgTime, 'n', msg.sendTime);
                        //console.log(intervalMin2);
                        if (intervalMin2 > 10) {
                            $(".GroupChatIndex_MsgPanel").append(msgTemplate.getTimeMsg(msgParam));
                        };
                    }
                } catch (e) {
                    //测试
                    //alert(e);
                }
            },
            message.getSystemMsg = function (msg) {
                var msghtml = '<div class="chatTime msgCls_' + msg.chatId + '"><span>' + msg.text + '</span></div>';
                return msghtml;
            },
            message.getTimeMsg = function (msg) {
                var msghtml = '<div class="chatTime msgCls_' + msg.chatId + '"><span>' + msg.text + '</span></div>';
                return msghtml;
            },
            //消息时间
            message.getTimeMsg = function (msg) {
                var msghtml = '<div class="chatTime"><span>' + msg.text + '</span></div>';
                return msghtml;
            }

        /********************对方发送的消息开始****************************/
        //文字消息
        message.getReceiveTextMsg = function (content) {
            content.text = content.text.replace(/\n/g, '<br/>');
            content.text = msgTemplate.replaceFace(content.text);
            content.text = msgTemplate.replaceLink(content.text);

            var msgInfo = ' <div class="pcChatLeft pcChatLeftallGroup clearfix">' +
                '           <div class="pcTopPic">' +
                '               <div class="pcTopWidth pcMsgHead">' +
                '                   <img src="' + content.senderHeadUrl + '" alt="">' +
                '               </div>' +
                '           </div>' +
                '           <div class="pcText">' +
                '               <div class="smallPass comonAllGroup ">' +
                '                   <h5 class="arrow"><em></em><span></span></h5>' +
                '                   <div>' +
                content.text +
                '                  </div>' +
                '                </div>' +
                '           </div>' +
                '      </div>';
            return msgInfo;
        },
            //图片消息
            message.getReceiveImgMsg = function (content) {
                var msgInfo = ' <div class="pcChatLeft pcChatLeftallGroup clearfix">' +
                    '          <div class="pcTopPic">' +
                    '              <div class="pcTopWidth pcMsgHead">' +
                    '                  <img src="' + content.senderHeadUrl + '" alt="">' +
                    '              </div>' +
                    '          </div>' +
                    '          <div class="pcText">' +
                    '              <div class="leftPic comonAllGroup ">' +
                    '                  <h5 class="arrow"><em></em><span></span></h5>' +
                    '                  <div class="imgZise picMax">' +
                    //'                      <div class="imgZiseLoading"></div>'+
                    '                      <img src="' + content.imgUrl + '"" alt="图片">' +
                    '                  </div>' +
                    '              </div>' +
                    '          </div>' +
                    '      </div>';
                return msgInfo;
            },
            //链接
            message.getReceiveLinkMsg = function (content) {
                var msgInfo = ' <div class="pcChatLeft pcChatLeftallGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '      <div class="pcTopPic">' +
                    '          <div class="pcTopWidth pcMsgHead">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>' +
                    '      </div>' +
                    '      <div class="pcText">' +
                    '           <div class="chart-left-title"> ' + msg.nickName + '<span class="msgTitleGroupName"></span></div>' +
                    '          <a ' + loadLinkUrl(msg.linkData.url) + ' target="_blank" class="pcLinkdMain"> ' +
                    '              <h5 class="arrow"><em></em><span></span></h5>' +
                    '              <div>' +
                    '                  <p class="linkTitle">' + overflowEllipsis(msg.linkData.title, 31) + '</p>' +
                    '                  <div class="linkText clearfix">' +
                    '	                    <aside class="link">' +
                    '	                    ' + overflowEllipsis(msg.linkData.desc, 32) +
                    '                     </aside>' +
                    '                     <h5>' +
                    '                     <img src="' + msg.linkData.iconUrl + '" alt="">' +
                    '                     </h5>' +
                    '                 </div>' +
                    '             </div>' +
                    '         </a>' +
                    '     </div>' +
                    '</div>';
                return msgInfo;
            },
            //小程序
            message.getReceiveXCXMsg = function (content) {
                var msgInfo = '<div class="pcChatLeft pcChatLeftallGroup  msgCls_' + msg.chatId + ' clearfix">' +


                    '      <div class="pcTopPic">' +
                    '          <div class="pcTopWidth pcMsgHead">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>' +
                    '      </div>' +
                    '      <div class="pcText">' +

                    '           <div class="chart-left-title"> ' + msg.nickName + '<span class="msgTitleGroupName"></span></div>' +
                    '          <a href="#" class="pcxiaodMain rightClickDocumentMenu"> ' +
                    '              <h5 class="arrow"><em></em><span></span></h5>' +
                    '              <div>' +
                    '                  <h4><img src="' + msg.xcxData.appIconUrl + '" alt=""><span>' + msg.xcxData.displayName + '</span></h4>' +
                    '                  <div class="xiaoText clearfix">' +
                    '                    <aside class="xiao">' + msg.xcxData.title + '</aside>' +
                    '                   <h5><img src="' + msg.xcxData.imgUrl + '" alt=""></h5>' +
                    '                  </div>' +
                    '                  <p class="xiaoTitle"><img src="/content/html/images/xiao.png" alt="">小程序</p>' +
                    '              </div>' +
                    '          </a>' +
                    '      </div>' +
                    '  </div>';

                return msgInfo;
            },
            //名片
            message.getReceiveCardMsg = function (content) {
                var msgInfo = '<div class="pcChatLeft pcChatLeftallGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '      <div class="pcTopPic">' +
                    '          <div class="pcTopWidth pcMsgHead">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>' +
                    '      </div>' +
                    '      <div class="pcText">' +
                    '           <div class="chart-left-title"> ' + msg.nickName + '<span class="msgTitleGroupName"></span></div>' +
                    '          <div class="pcCardMain"> ' +
                    '              <h5 class="arrow"><em></em><span></span></h5>' +
                    '              <div>' +
                    '                  <div class="cardText clearfix">' +
                    '                    <aside class="card">' +
                    '	                    <b>' + msg.cardData.nickName + '</b>' +
                    '	                    <span>' + overflowEllipsis(msg.cardData.wxNum, 11) + '</span>' +
                    '	                    </aside>' +
                    '	                    <h5><img src="' + msg.cardData.headUrl + '" alt=""></h5>' +
                    '                    </div>' +
                    '                     <p class="cardTitle">个人名片</p>' +
                    '                </div>' +
                    '            </div>' +
                    '       </div>' +
                    '   </div>';
                return msgInfo;
            },
            //语音
            message.getReceiveVoiceMsg = function (content) {
                var msgInfo = '<div class="pcChatLeft pcChatLeftallGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '      <div class="pcTopPic">' +
                    '          <div class="pcTopWidth pcMsgHead">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>' +
                    '      </div>' +
                    '      <div class="pcText">' +
                    '           <div class="chart-left-title"> ' + msg.nickName + '<span class="msgTitleGroupName"></span></div>' +
                    '          <div class="videoPass rightPass" style="width: ' + voiceWeight(msg.VoiceDuration) + ';"> ' +
                    '              <h5 class="arrow"><em></em><span></span></h5>' +
                    '              <span class="videoBtn ting"><img src="/images/voice.png" alt=""></span>	 ' +
                    '              <audio id="msgVoice_' + msg.chatId + '" class="msgVoice">' +
                    '                   <source src="' + msg.fileUrl + '">' +
                    '              </audio>' +
                    '          </div>' +
                    "          <div class='videoDuration'>" + msg.VoiceDuration + "''</div>"
                '      </div>' +
                    '  </div>';

                return msgInfo;
            },
            //视频
            message.getReceiveVideoMsg = function (content) {
                var msgInfo = '<div class="pcChatLeft pcChatLeftallGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '      <div class="pcTopPic">' +
                    '          <div class="pcTopWidth pcMsgHead">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>' +
                    '      </div>' +
                    '      <div class="pcText">' +
                    '           <div class="chart-left-title"> ' + msg.nickName + '<span class="msgTitleGroupName"></span></div>' +
                    '          <div class="leftPic comonAllGroup "> ' +
                    '              <h5 class="arrow"><em></em><span></span></h5>' +
                    '              <div class="charVideo">' +
                    '                  <p><img src="/content/html/images/chatvideo.png" alt=""></p>' +
                    '                  <img src="' + msg.fileUrl + '" alt="图片">' +
                    '              </div>' +
                    //'              <div class="chatvideoBg"></div>' +
                    '          </div>' +
                    '      </div>' +
                    '</div>';
                return msgInfo;
            },
            //红包
            message.getReceiveRedPacketMsg = function (content) {
                var msgInfo = '<div class="pcChatLeft pcChatLeftallGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '      <div class="pcTopPic">' +
                    '          <div class="pcTopWidth pcMsgHead">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>' +
                    '      </div>' +
                    '      <div class="pcText">' +
                    '           <div class="chart-left-title"> ' + msg.nickName + '<span class="msgTitleGroupName"></span></div>' +
                    '          <div class=" leftPic comonAllGroup "> ' +
                    '              <h5 class="arrow"><em></em><span></span></h5>' +
                    '              <div class="WeChatRed">' +
                    '                  <p>' + msg.text + '</p>' +
                    '                  <img src="/images/timgBg.png" alt="">' +
                    '              </div>' +
                    '          </div>' +
                    '      </div>' +
                    '  </div>';
                return msgInfo;
            },
            //转账
            message.getReceiveTransferMsg = function (content) {
                //转账状态(0:未领取  30:已领取  40:已退回)
                var statusCode = msg.StatusCode;
                var title = msg.TransferData.Title;
                //转账金额
                var feeSum = msg.TransferData.FeeSum;
                //转账说明
                var payMemo = msg.TransferData.PayMemo;

                var msgInfo = '<div class="pcChatLeft pcChatLeftallGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '      <div class="pcTopPic">' +
                    '          <div class="pcTopWidth pcMsgHead">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>' +
                    '      </div>' +
                    '      <div class="pcText">' +
                    '           <div class="chart-left-title"> ' + msg.nickName + '<span class="msgTitleGroupName"></span></div>' +
                    '          <div class=" leftPic comonAllGroup "> ' +
                    '              <h5 class="arrow"><em></em><span></span></h5>' +
                    '              <div class="WeChatRed">' +
                    '                  <p class="desc">' + getMsgTransferDetails(statusCode, payMemo, 0) + '</p>' +
                    '                  <p class="amount">' + feeSum + '</p>' +
                    '                  <img src="' + getMsgTransferStatus(statusCode, 0) + '" alt="">' +
                    '              </div>' +
                    '          </div>' +
                    '      </div>' +
                    '  </div>';
                return msgInfo;
            },
            /********************对方发送的消息结束****************************/

            /********************本方发出的消息开始****************************/
            //文字消息
            message.getSendTextMsg = function (content) {
                content.text = content.text.replace(/\n/g, '<br/>');
                content.text = msgTemplate.replaceFace(content.text);
                content.text = msgTemplate.replaceLink(content.text);

                //var sendStatus = getSendStatus(msg);

                var msgInfo = '<div class="pcRightList pcRightListGroup clearfix">' +
                    '          <div class="pcTopRight">' +
                    '              <div class="pcTopWidth">' +
                    '                  <img src="' + content.senderHeadUrl + '" alt="">' +
                    '              </div>' +
                    '          </div>' +
                    '          <div class="pcRightText">' +
                    '              <div class="rightPass comonAllGroup ">' +
                    //'              ' + sendStatus +
                    '                  <h5 class="arrow"><em></em><span></span></h5>' +
                    '                  <div>' + content.text + '</div>' +
                    '          </div>' +
                    '      </div>' +
                    '</div>';
                return msgInfo;
            },
            //图片消息
            message.getSendImgMsg = function (content) {
                //var sendStatus = getSendStatus(content);

                var msgInfo = '<div class="pcRightList pcRightListGroup clearfix">' +
                    '           <div class="pcTopRight">' +
                    '               <div class="pcTopWidth">' +
                    '                   <img src="' + content.senderHeadUrl + '" alt="">' +
                    '               </div>' +
                    '           </div>' +
                    '           <div class="pcRightText">' +
                    '               <div class="rightPass comonAllGroup ">' +
                    '                   <h5 class="arrow"><em></em><span></span></h5>' +
                    '                   <div class="imgZise picMax">' +
                    '                       <img src="' + content.imgUrl + '" alt="图片">' +
                    '                   </div>' +
                    '               </div>' +
                    '           </div>' +
                    '       </div>';
                return msgInfo;
            },
            //链接
            message.getSendLinkMsg = function (content) {
                var sendStatus = getSendStatus(msg);

                var msgInfo = '<div class="pcRightList pcRightListGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '      <div class="pcTopRight">' +
                    '          <div class="pcTopWidth">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>	' +
                    '      </div>' +
                    '      <a ' + loadLinkUrl(msg.linkData.url) + ' target="_blank"  class="RightLinkdMain comonAllGroup"> ' +
                    '              ' + sendStatus +
                    '          <h5 class="arrow"><em></em><span></span></h5>' +
                    '          <div>' +
                    '              <p class="linkTitle">' + overflowEllipsis(msg.linkData.title, 31) + '</p>' +
                    '              <div class="linkText clearfix">' +
                    '                  <aside class="link">' +
                    '	                    ' + overflowEllipsis(msg.linkData.desc, 32) +
                    '      </aside>' +
                    '      <h5><img src="' + msg.linkData.iconUrl + '" alt=""></h5>' +
                    '  </div>' +
                    '  </div>' +
                    '  </a>' +
                    '  <div class="picBg"></div>' +
                    '  </div>';
                return msgInfo;
            },
            //小程序
            message.getSendXCXMsg = function (content) {
                var sendStatus = getSendStatus(msg);

                var msgInfo = '<div class="pcRightList pcRightListGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '      <div class="pcTopRight">' +
                    '          <div class="pcTopWidth">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>	' +
                    '      </div>' +
                    '      <a href="#" class="RightxiaodMain ClickDocumentMenu"> ' +
                    '              ' + sendStatus +

                    '          <h5 class="arrow"><em></em><span></span></h5>' +
                    '          <div>' +
                    '              <h4><img src="' + msg.xcxData.appIconUrl + '" alt=""><span>' + msg.xcxData.displayName + '</span></h4>' +
                    '              <div class="xiaoText clearfix">			' +
                    '                  <aside class="xiao">' + msg.xcxData.title + '</aside>' +
                    '                  <h5><img src="' + msg.xcxData.imgUrl + '" alt=""></h5>' +
                    '              </div>' +
                    '              <p class="xiaoTitle"><img src="/content/html/images/xiao.png" alt="">小程序</p>' +
                    '          </div>' +
                    '      </a>' +
                    '      <div class="picBg"></div>' +
                    '  </div>';

                return msgInfo;
            },
            //名片
            message.getSendCardMsg = function (content) {
                var sendStatus = getSendStatus(content);

                var msgInfo = '<div class="pcRightList pcRightListGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '      <div class="pcTopRight">' +
                    '          <div class="pcTopWidth">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>	' +
                    '      </div>' +
                    '      <div class="pcCardMain comonAllGroup"> ' +
                    '              ' + sendStatus +
                    '          <h5 class="arrow"><em></em><span></span></h5>' +
                    '          <div>' +
                    '              <div class="cardText clearfix">' +
                    '                  <aside class="card">' +
                    '                     <b>' + msg.cardData.nickName + '</b>' +
                    '                    <span>' + overflowEllipsis(msg.cardData.wxNum, 11) + '</span>' +
                    '                  </aside>' +
                    '                  <h5><img src="' + msg.cardData.headUrl + '" alt=""></h5>' +
                    '              </div>' +
                    '              <p class="cardTitle">个人名片</p>' +
                    '          </div>' +
                    '      </div>' +
                    '  </div>';

                return msgInfo;
            },
            //语音
            message.getSendVoiceMsg = function (content) {
                var sendStatus = getSendStatus(content);

                var msgInfo = '<div class="pcRightList clearfix msgCls_' + msg.chatId + '">' +
                    '      <div class="pcTopRight">' +
                    '          <div class="pcTopWidth">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>	' +
                    '      </div>' +
                    '      <div class="pcRightText">' +
                    '              <div class="videoPass rightPass comonAllGroup" style="width: ' + voiceWeight(msg.VoiceDuration) + ';">' +
                    '              ' + sendStatus +
                    '                  <h5 class="arrow"><em></em><span></span></h5>' +
                    '                  <span class="videoRightBtn tingRight"><img src="/content/html/images/voice1.png" alt=""></span>	 ' +
                    '                  <audio id="video3" class="video">' +
                    '                     <source src="' + msg.fileUrl + '">' +
                    '                  </audio>' +
                    '              </div>' +
                    "              <div class='videoDuration'>" + msg.VoiceDuration + "''</div>" +
                    '      </div>' +
                    '  </div>';
                return msgInfo;
            },
            //视频
            message.getSendVideoMsg = function (content) {
                var sendStatus = getSendStatus(content);

                var msgInfo = '<div class="pcRightList pcRightListGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '           <div class="pcTopRight">' +
                    '               <div class="pcTopWidth">' +
                    '                   <img src="' + msg.headUrl + '" alt="">' +
                    '               </div>' +
                    '           </div>' +
                    '           <div class="pcRightText">' +
                    '               <div class="rightPass comonAllGroup ">' +
                    '              ' + sendStatus +
                    '                   <h5 class="arrow"><em></em><span></span></h5>' +
                    '                   <div class="imgZise charVideo picMax">' +
                    '                       <p><img src="/images/chatvideo.png" alt=""></p>' +
                    '                       <img src="' + msg.fileUrl + '" alt="你的视频">' +
                    '                   </div>' +
                    //'                   <div class="imgZise picMax">' +
                    //'                       <img src="' + msg.FileUrl + '" alt="你的图片">' +
                    //'                   </div>' +
                    '               </div>' +
                    '           </div>' +
                    '       </div>';
                return msgInfo;
            },
            //转账
            message.getSendTransferMsg = function (content, chatRoomId) {
                //转账状态(0:未领取  30:已领取  40:已退回)
                var statusCode = msg.StatusCode;
                var title = msg.TransferData.Title;
                //转账金额
                var feeSum = msg.TransferData.FeeSum;
                //转账说明
                var payMemo = msg.TransferData.PayMemo;

                var msgInfo = '<div class="pcRightList pcRightListGroup msgCls_' + msg.chatId + ' clearfix">' +
                    '      <div class="pcTopRight">' +
                    '          <div class="pcTopWidth pcMsgHead">' +
                    '              <img src="' + msg.headUrl + '" alt="">' +
                    '          </div>' +
                    '      </div>' +
                    '      <div class="pcText">' +
                    '           <div class="chart-left-title"> ' + msg.nickName + '<span class="msgTitleGroupName"></span></div>' +
                    '          <div class=" rightPic comonAllGroup "> ' +
                    '              <h5 class="arrow"><em></em><span></span></h5>' +
                    '              <div class="WeChatRed">' +
                    '                  <p class="desc">' + getMsgTransferDetails(statusCode, payMemo, 1) + '</p>' +
                    '                  <p class="amount">' + feeSum + '</p>' +
                    '                  <img src="' + getMsgTransferStatus(statusCode, 1) + '" alt="">' +
                    '              </div>' +
                    '          </div>' +
                    '      </div>' +
                    '  </div>';
                return msgInfo;
            },
            /********************本方发出的消息结束****************************/

            message.replaceFace = function (text) {
                try {
                    if (FaceList == null || FaceList.length <= 0) {
                        $.ajax({
                            dataType: 'json',
                            url: '/scripts/common/qqFace.js',
                            async: false,
                            success: function (response) {
                                FaceList = response;
                            }
                        });
                    }

                    var reg = new RegExp('\\[(.+?)\\]', "g");
                    var arr = text.match(reg);

                    if (arr != null && arr.length > 0) {
                        text = text.replace(/\[(.+?)\]/g, function (match) {
                            var face = FaceList.filter(function (val) {
                                return val['value'] == match;
                            });

                            var faceImg = match;
                            if (face != null && face.length > 0) {
                                faceImg = '<img src="' + face[0].icon + '" width="22" height="22" style="display:inline-block" />';
                            }
                            return faceImg;
                        });
                    };

                } catch (e) {

                }
                return text;
            },
            message.replaceLink = function (txt) {
                var reg = /(((https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/ig;
                var result = txt.replace(reg, function (item) {
                    var itemLink = item;
                    if (item.indexOf('http') != 0) {
                        itemLink = "//" + item;
                    }
                    return "<a href='" + itemLink + "' target='_blank'>" + item + "</a>";
                });
                return result;
            },
            //显示消息发送时间
            //规则：
            //1. 一天以前的消息显示 年/月/日
            //2. 今天内的消息显示 时分
            message.getMenuMsgTime = function (msgTime) {
                var newTime = "";
                try {
                    if (msgTime != "" && msgTime != "2000-01-01T00:00:00") {
                        var today = UtilDateKit.getNow();

                        var timeFormat = "HH:mm";
                        //一天前的消息格式
                        //console.log("消息时间:" + msgTime + ",当前时间:" + today);
                        if (msgTime < today) {
                            timeFormat = "yy/M/D";
                        };
                        //var timeFormat = "yy/M/D HH:mm";
                        msgTime = msgTime.replace(/T/g, ' ');
                        msgTime = msgTime.replace(/-/g, "/");
                        newTime = UtilDateKit.format(msgTime, timeFormat);
                    }
                } catch (e) {

                }
                return newTime;
            }

        var loadLinkUrl = function (url) {
            if (url != '') {
                return ' href = "' + url + '"';
            }
            return '';
        }

        var voiceWeight = function (voiceDuration) {
            var voiceW = '40px';
            if (voiceDuration <= 2) {
                voiceW = '45px';
            }
            else if (voiceDuration <= 5) {
                voiceW = '55px';
            }
            else if (voiceDuration <= 7) {
                voiceW = '60px';
            }
            else if (voiceDuration <= 10) {
                voiceW = '65px';
            }
            else if (voiceDuration <= 20) {
                voiceW = '75px';
            }
            else if (voiceDuration <= 30) {
                voiceW = '85px';
            }
            else if (voiceDuration <= 40) {
                voiceW = '95px';
            }
            else if (voiceDuration <= 60) {
                voiceW = '110px';
            }
            else if (voiceDuration > 60) {
                voiceW = '130px';
            }
            return voiceW;
        }

        var getSendStatus = function (msg) {
            //success: 0 未发送 1 已发送 4发送失败
            //设置加载中图标
            var sendStatus = '';
            if (msg.SendCode == 1 && msg.Success == 0) {
                sendStatus = '<div title="' + UtilDateKit.format(msg.sendTime, "yyyy年M月D日 hh:mm") + '" class="goTimerRight loadMsgStatusFlag"></div>';
            }
            else if (msg.Success == 0) {
                sendStatus = '<div class="goLoadRight loadMsgStatusFlag"></div>';
            }
            else if (msg.Success == 4) {
                sendStatus = '<div class="goLoseRight loadMsgStatusFlag"></div>';
            }

            return sendStatus;
        }

        var getTimestamp = function (msg, timestamp) {
            var clsTimestamp = '';
            if (timestamp != null && timestamp != "") {
                clsTimestamp = "txtMsg_" + timestamp;
            }
            else {
                clsTimestamp = "txtMsg_" + msg.ChatId;
            }

            return clsTimestamp;
        }

        var overflowEllipsis = function (text, num) {
            if (text == null || text == "" || text == undefined) {
                return "";
            }
            var maxwidth = num;
            if (text.length > maxwidth) {
                return text.substring(0, maxwidth) + '...';
            }
            return text;
        }
        var getMsgTransferStatus = function (status, type) {
            //转账状态(0:未领取  30:已领取  40:已退回)
            var img_src = ''
            if (status == 0 && type == 0) {
                img_src = "/images/transferLeft.png";//对方已转账
            } else if (status == 0 && type == 1) {
                img_src = "/images/transferLeft.png";//本方已转账
            } else if (status == 30 && type == 0) {
                img_src = "/images/receiptL.png";//对方已转账
            } else if (status == 30 && type == 1) {
                img_src = "/images/receiptR.png";//对方已转账
            } else if (status == 40 && type == 0) {
                img_src = "/images/backL.png";//对方已转账
            } else if (status == 40 && type == 1) {
                img_src = "/images/backR.png";//对方已转账
            }
            return img_src;
        }
        var getMsgTransferDetails = function (status, payMemo, type) {
            //转账状态(0:未领取  30:已领取  40:已退回)
            var content_list = '转账给你'
            if (type == 0) {
                if (status == 0) {
                    content_list = "转账给你";
                }
                else if (status == 30) {
                    content_list = "已被领取";
                }
                else if (status == 40) {
                    content_list = "已退还";
                }
                if (payMemo != '') {
                    content_list = content_list + '- ' + payMemo
                }
            }
            else {
                if (status == 0) {
                    content_list = "转账给对方";
                }
                else if (status == 30) {
                    content_list = "已收钱";
                }
                else if (status == 40) {
                    content_list = "已退还";
                }
            }
            return content_list;
        }

        return message;
    })();

    //输出test接口
    exports('msgTemplate', msgTemplate);
});