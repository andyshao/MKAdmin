﻿layui.define(['jquery', 'layer'], function (exports) {
    var $ = layui.jquery;
    var layer = layui.layer;
    /**/
    var copyBoardUtil = {
        copyUtil: function (cl, imgClass) {
                var SelectText = function (element) {
                var doc = document;
                if (doc.body.createTextRange) {
                    var range = document.body.createTextRange();
                    range.moveToElementText(element);
                    range.select();
                } else if (window.getSelection) {
                    var selection = window.getSelection();
                    var range = document.createRange();
                    range.selectNodeContents(element);
                    selection.removeAllRanges();
                    selection.addRange(range);
                }
            }
            
            $(cl).click = null;
            $(cl).click(function () {
              
                $(imgClass).attr("contenteditable", true);
                SelectText($(imgClass).get(0));
                document.execCommand('copy');
                window.getSelection().removeAllRanges();
                $(imgClass).removeAttr("contenteditable");
                //alert('复制图片成功');
                layer.msg('复制图片成功', {icon:6})
            })
        },
        ImgBox: function (_this, url) {
            var imgHeight = _this.next('img').height();//图片宽高度 
            var imgWidth = _this.next('img').width();//图片宽高度 
            var widW = $(window).width(); //窗高度 宽度
            var widH = $(window).height();

            $(window).resize(function () {
                widW = $(window).width(); //窗高度 宽度
                widH = $(window).height();
            });

            if (imgHeight > widH-150) {//判断图片大于窗口高
                imgHeight = widH - 150
            }
            if (imgWidth > widW - 180) {//判断图片大于窗口宽
                imgWidth = widW - 180
            }
            layer.open({
                type: 1,
                title: false,
                closeBtn: 1,
                shadeClose: true,
                area: [imgWidth + 'px', imgHeight + 'px'], //宽高
                content: "<img alt=" + name + " title=" + name + " src=" + url + " />"
            });
        }
    }

    //输出test接口
    exports('copyBoardUtil', copyBoardUtil);
});
