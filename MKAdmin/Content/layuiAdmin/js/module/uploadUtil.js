﻿//上传公共库
layui.define(['jquery', 'dialog', 'upload'], function (exports) {
    var $ = layui.jquery
        , dialog = layui.dialog
        , upload = layui.upload;

    var uploadUtil = {
        /**/
        render: function (jsonData) {
            var parameter = {
                elemId: ''               //上传按钮Id
                , data: {}               //上传参数
                , url: '/home/uploadfile'               //上传接口地址
                , accept: 'file'        //images（图片）、file（所有文件）、video（视频）、audio（音频）
                //, acceptMime: 'image/jpeg' //文件映射
                //允许上传的文件后缀。一般结合 accept 参数类设定。
                //假设 accept 为 file 类型时，那么你设置 exts: 'zip|rar|7z' 
                //即代表只允许上传压缩格式的文件。如果 accept 未设定，那么限制的就是图片的文件格式
                , exts: 'jpg|png|gif|bmp|jpeg'
                , multiple: false       //是否允许多文件上传。设置 true即可开启。不支持ie8/9
                , number: 0             //设置同时可上传的文件数量，一般配合 multiple 参数出现。0（即不限制）
                , size: 0               //设置文件最大可允许上传的大小，单位 KB。不支持ie8/9。  0（即不限制）
                , done: function (res) {

                }
                , error: function () {
                    dialog.alert.fail('上传失败,请稍后重试...');
                }
            }

            $.extend(parameter, jsonData);

            var acceptMime = getFileAcceptMime(parameter.exts);

            upload.render({
                elem: '#' + parameter.elemId
                , url: parameter.url
                , acceptMime: acceptMime
                , multiple: parameter.multiple
                , number: parameter.number
                , data: parameter.data
                , exts: parameter.exts
                , size: parameter.size
                , before: function () {
                    dialog.alert.postLoading();
                }
                , done: function (res) { //上传后的回调
                    dialog.closeAll('loading');

                    if (res.status) {
                        return parameter.done(res);
                    }
                    else {
                        dialog.alert.fail(res.msg);
                    }
                }
                , error: function () {
                    dialog.closeAll('loading');
                }
            })
        }
    };

    var fileSuffixList = [
        { name: 'jpg', value: 'image/jpeg' }
        , { name: 'png', value: 'image/png' }
        , { name: 'png', value: 'application/x-png' }
        , { name: 'jpeg', value: 'image/jpeg' }
        , { name: 'jfif', value: 'image/jpeg' }
        , { name: 'bmp', value: 'application/x-bmp' }
        , { name: 'gif', value: 'image/gif' }
        , { name: 'ico', value: 'image/x-icon' }
        , { name: 'txt', value: 'text/plain' }
        , { name: 'doc', value: 'application/msword' }
        , { name: 'docx', value: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' }
        , { name: 'xls', value: 'application/x-xls' }
        , { name: 'xls', value: 'application/vnd.ms-excel' }
        , { name: 'xlsx', value: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }
        , { name: 'ppt', value: 'application/x-ppt' }
        , { name: 'pptx', value: 'application/vnd.openxmlformats-officedocument.presentationml.presentation' }
        , { name: 'ppt', value: 'application/vnd.ms-powerpoint' }
        , { name: 'rar', value: 'application/x-rar-compressed' }
        , { name: 'zip', value: 'application/zip' }
        , { name: 'apk', value: 'application/vnd.andriod' }
        , { name: 'mp3', value: 'audio/x-mpeg' }
        , { name: 'mp4', value: 'video/mp4' }
        , { name: 'amr', value: 'audio/amr' }
    ];

    var getFileAcceptMime = function (suffix) {
        var acceptMime = [];

        var suffixSplit = suffix.split('|');
        $.each(suffixSplit, function (index, item) {
            var suffixMatch = fileSuffixList.filter(function (itemf) {
                return itemf.name == item;
            });
            if (suffixMatch != null && suffixMatch.length > 0) {
                $.each(suffixMatch, function (indexSuffix, itemSuffix) {
                    if (acceptMime.indexOf(itemSuffix.value) < 0) {
                        acceptMime.push(itemSuffix.value);
                    }
                });
            }
        });

        return acceptMime.join(',');
    }
    //输出
    exports('uploadUtil', uploadUtil);
});
