﻿using MKAdmin.Web.Authorizes;
using MKAdmin.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKAdmin.Web.Controllers
{
    [MvcAuthorize]
    public class BaseMvcController : Controller
    {

    }
}