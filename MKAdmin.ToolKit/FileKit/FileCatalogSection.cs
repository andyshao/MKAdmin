﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ToolKit.FileKit
{
    public class FileCatalogSection : ConfigurationSection
    {
        /// <summary>
        /// Catalog 集合节点
        /// </summary>
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public CatalogCollection Catalog
        {
            get
            {
                return (CatalogCollection)base[""];
            }
        }
    }
}
