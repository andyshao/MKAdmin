﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ToolKit.FileKit
{
    public class CatalogElement : ConfigurationElement
    {
        /// <summary>
        /// 键（唯一性） 读取该节点的标识
        /// </summary>
        [ConfigurationProperty("Key")]
        public string Key
        {
            get
            {
                return (string)this["Key"];
            }
            set
            {
                base["Key"] = value;
            }
        }

        /// <summary>
        /// 文件大小 单位：MB
        /// </summary>
        [ConfigurationProperty("FileSize")]
        public string FileSize
        {
            get
            {
                return (string)this["FileSize"];
            }
            set
            {
                base["FileSize"] = value;
            }
        }

        /// <summary>
        /// 文件类型限制
        /// </summary>
        [ConfigurationProperty("FileTypeLimit")]
        public string FileTypeLimit
        {
            get
            {
                return (string)this["FileTypeLimit"];
            }
            set
            {
                base["FileTypeLimit"] = value;
            }
        }

        /// <summary>
        /// 临时路径节点
        /// </summary>
        [ConfigurationProperty("TempFile")]
        public TempFileElement TempFile { get { return (TempFileElement)base["TempFile"]; } }

        /// <summary>
        /// 目标路径节点
        /// </summary>
        [ConfigurationProperty("DestFile")]
        public DestFileElement DestFile { get { return (DestFileElement)base["DestFile"]; } }
    }
}
