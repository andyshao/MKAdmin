﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKAdmin.ToolKit.FileKit
{
    public class FileCatalogHelper
    {
        /// <summary>
        /// 文件目录 配置信息
        /// </summary>
        private readonly static FileCatalog _fileCatalog = new FileCatalog();

        /// <summary>
        /// 获取配置Catalog节点信息
        /// </summary>
        public static FileCatalog Catalog
        {
            get
            {
                return _fileCatalog;
            }
        }
    }

    /// <summary>
    /// 文件目录 配置信息
    /// </summary>
    public class FileCatalog
    {
        /// <summary>
        /// 主 节点名称
        /// </summary>
        private const string SECTION_NAME = "FileKit";

        /// <summary>
        /// 文件目录 节点信息
        /// </summary>
        private readonly static FileCatalogSection _config = null;
        /// <summary>
        /// 出始 信息
        /// </summary>
        static FileCatalog()
        {
            _config = (FileCatalogSection)System.Configuration.ConfigurationManager.GetSection(SECTION_NAME);
        }

        /// <summary>
        /// 禁止在外部程序集中实例化
        /// </summary>
        internal FileCatalog()
        {

        }

        /// <summary>
        /// 索引器（文件目录）,Catalog节点集合
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>CatalogElement</returns>
        public CatalogElement this[string key]
        {
            get
            {
                return _config.Catalog[key];
            }
        }

    }
}
