﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace MKAdmin.ToolKit.CacheHelper
{
    /// <summary>
    /// 缓存管理(C#自带缓存)
    /// </summary>
    public class CacheHelper
    {
        private static System.Web.Caching.Cache cache = HttpRuntime.Cache;
        /// <summary>
        /// 缓存名称-用户权限
        /// </summary>
        public static string CacheName_UserAuthorize = "CacheName_UserAuthorize_";

        /// <summary>
        /// 添加缓存，如果存在则抛出异常
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expireTime">过期时间</param>
        /// <returns></returns>
        public static void Add(string key, object value, DateTime expireTime)
        {
            cache.Add(key, value, null, expireTime, System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
        }
        /// <summary>
        /// 根据键值获得缓存值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object Get(string key)
        {
            return cache[key];
        }
        /// <summary>
        /// 添加缓存，如果存在则抛出异常
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static void Add(string key, object value)
        {
            cache.Add(key, value, null, DateTime.MaxValue, System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
        }
        /// <summary>
        /// 移除缓存
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static void Remove(string key)
        {
            cache.Remove(key);
        }
    }
}
