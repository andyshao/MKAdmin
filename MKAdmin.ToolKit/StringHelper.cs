﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MKAdmin.ToolKit
{
    public static class StringHelper
    {
        /// <summary>
        /// 获取接口错误描述信息
        /// </summary>
        /// <param name="apiUrl"></param>
        /// <param name="parameterInfo"></param>
        /// <returns></returns>
        public static string GetApiErrorContent<T>(string apiUrl, T parameter)
        {
            StringBuilder sd = new StringBuilder();
            sd.AppendLine("调用接口异常：");
            sd.AppendLine("接口名称：" + apiUrl);
            if (parameter != null)
            {
                sd.AppendLine("参数信息：" + JsonConvert.SerializeObject(parameter));
            }
            else
            {
                sd.AppendLine("参数信息：未获取到参数信息");
            }

            return sd.ToString();
        }
    }
}
