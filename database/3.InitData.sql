--delete from [EmployeeInfo]

--INSERT INTO [dbo].[EmployeeInfo]
--           ([EmployeeName],[EmployeeSex],[EmployeeAge],[EmployeeHeight]
--		   ,[EmployeeWeight],[PayMoney],[HeadFileName],[ContactPhone]
--           ,[Education],[AddressDetail],[Hobby],[Motto]
--		   ,CreatorId,[StatusCode],[EntryTime],[CreateTime]
--		   ,[UpdateTime],[Remark])
--     VALUES
--           ('阿麦',1,25,176
--           ,75,35000,'','15845875421'
--           ,'本科','广东省南山区西丽','编码','梦想不一定能实现，但一定要有'
--           ,0,0,getdate(),getdate()
--		   ,getdate(),'')
--		   ,('莉莉',2,23,165
--           ,55,25000,'','15946875434'
--           ,'本科','广东省南山区西丽','跳舞','...'
--           ,0,0,getdate(),getdate()
--		   ,getdate(),'')
--		   ,('刘凯',1,28,170
--           ,55,25000,'','1576458424'
--           ,'博士','江西省南昌市','唱歌','...'
--           ,0,0,getdate(),getdate()
--		   ,getdate(),'')
	
--GO

declare @i int
set @i = 101;
while(@i <= 200)
begin
 INSERT INTO [dbo].[EmployeeInfo]
           ([EmployeeName],[EmployeeSex],[EmployeeAge],[EmployeeHeight]
		   ,[EmployeeWeight],[PayMoney],[HeadFileName],[ContactPhone]
           ,[Education],[AddressDetail],[Hobby],[Motto]
           ,[StatusCode],[EntryTime],[CreateTime],[UpdateTime]
           ,[Remark],[Status],CreatorId)
     VALUES
           ('阿麦'+Cast(@i as varchar(5)),1,25,176
           ,75,35000,'','15845875421'
           ,'本科','广东省南山区西丽','编码','梦想不一定能实现，但一定要有'
           ,0,getdate(),getdate(),getdate()
           ,'',0,0)
 set @i+=1;
end

